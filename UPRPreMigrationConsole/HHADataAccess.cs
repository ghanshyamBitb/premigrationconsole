﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Collections;
using System.IO;
using System.Data.SqlClient;

namespace UPRPreMigrationConsole.Common
{
    public class HHADataAccess
    {

        private static Hashtable LoadConfiguration()
        {
            string basePath = string.Empty;

            basePath = AppDomain.CurrentDomain.BaseDirectory;

            string mappingFile = string.Format(@"{0}\ProcedureMapping.xml", basePath);

            if (System.IO.File.Exists(mappingFile))
            {
                ProcedureConfiguration NECImporter = new ProcedureConfiguration();
                XmlSerializer pcSerializer = new XmlSerializer(typeof(ProcedureConfiguration));

                Hashtable commands = new Hashtable();

                ProcedureConfiguration pc = null;

                using (StreamReader xmlReader = new StreamReader(mappingFile))
                {
                    pc = (ProcedureConfiguration)(pcSerializer.Deserialize(xmlReader));
                }

                foreach (var item in pc.Mappings)
                {
                    if (string.IsNullOrWhiteSpace(item.ConnectionString)
                        || string.IsNullOrWhiteSpace(item.ProcName)
                        || string.IsNullOrWhiteSpace(item.Key)
                        //|| ConfigurationManager.ConnectionStrings[item.ConnectionString] == null
                        )
                    {
                        continue;
                    }

                    if (item.CommandTimeout <= 0)
                        item.CommandTimeout = 30;

                    if (!string.IsNullOrWhiteSpace(item.Key))
                    {
                        if (!commands.ContainsKey(item.Key))
                        {
                            commands.Add(item.Key, item);
                        }
                    }

                }
                return commands;
            }

            throw new FileNotFoundException("Procedure mapping file 'ProcedureMapping.xml' not found");
        }

        public static CommandInfo GetCommandDetails(string key)
        {
            CommandInfo cmd = new CommandInfo();
            Hashtable hs = null;

            //if (!isUnitTestContext)
            //    hs = System.Web.HttpRuntime.Cache["DDA_ProcedureMapping"] as Hashtable;

            //if (hs == null)
            //{
            hs = LoadConfiguration();
            //    if (!isUnitTestContext)
            //        System.Web.HttpRuntime.Cache.Insert("DDA_ProcedureMapping", hs, null, DateTime.Now.AddHours(8), System.Web.Caching.Cache.NoSlidingExpiration);
            //}

            if (hs != null && hs.ContainsKey(key))
            {
                cmd = hs[key] as CommandInfo;
                return cmd;
            }

            return null;
        }
    }

    [Serializable()]
    [XmlRoot(ElementName = "ProcedureConfiguration")]
    public class ProcedureConfiguration
    {
        [XmlArray("Mappings")]
        [XmlArrayItem("Procedure", typeof(CommandInfo))]
        public List<CommandInfo> Mappings { get; set; }
    }

    [Serializable()]
    public class CommandInfo
    {
        [System.Xml.Serialization.XmlAttribute("Key")]
        public string Key { get; set; }
        [System.Xml.Serialization.XmlAttribute("ConnectionString")]
        public string ConnectionString { get; set; }
        [System.Xml.Serialization.XmlAttribute("ProcName")]
        public string ProcName { get; set; }
        [System.Xml.Serialization.XmlAttribute("CommandTimeout")]
        public int CommandTimeout { get; set; }
    }

    public static class TypeConverters
    {

        public static T GetValue<T>(this SqlDataReader sqlDataReader, string columnName)
        {
            int columnIndex = 0;
            #region GET COLUMN INDEX | SET -1 IF COLUMN NOT FOUND
            try
            {
                columnIndex = sqlDataReader.GetOrdinal(columnName);
            }
            catch
            {
                columnIndex = -1;
            }
            #endregion

            if (columnIndex != -1 && !sqlDataReader.IsDBNull(columnIndex))
            {
                Type type = (Nullable.GetUnderlyingType(typeof(T)) != null) ? Nullable.GetUnderlyingType(typeof(T)) : typeof(T);
                return (T)Convert.ChangeType(sqlDataReader[columnName], type);
            }
            return default(T);

        }
    }





}
