﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UPRPreMigrationConsole.Common;

namespace UPRPreMigrationConsole
{
   public class Utility
    {
        public int UpdateMigratedRecordCount(int UserID, int PayerID, int ProviderID, DataTable MigrationTablesRecord, string Status)
        {
            CommandInfo cmdInfo = HHADataAccess.GetCommandDetails("DataMigration.Release.SaveMigrationTablesRecordCount");
            if (cmdInfo == null)
            {
                throw new NullReferenceException("CommandInfo not found for UpdateMigratedRecordCount()");
            }
            int processComplete = 0;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@UserID", UserID));
            parameters.Add(new SqlParameter("@PayerID", PayerID));
            parameters.Add(new SqlParameter("@ProviderID", ProviderID));
            parameters.Add(new SqlParameter("@MigrationTablesRecord", MigrationTablesRecord));
            parameters.Add(new SqlParameter("@Status", Status));
            parameters.Add(new SqlParameter("@Callerinfo", "UpdateMigratedRecordCount()"));

            using (SqlConnection con = new SqlConnection(AppSettings.Instance[cmdInfo.ConnectionString]))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandTimeout = cmdInfo.CommandTimeout;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = cmdInfo.ProcName;
                    cmd.Parameters.AddRange(parameters.ToArray());

                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    processComplete = 1;

                }
            }

            return processComplete;
        }

        public string GetMigrationValidationDetails(int userID, int providerID, int payerID, out bool isValidate)
        {
            isValidate = true;
            string validation = string.Empty, isEnableAreaRateg = string.Empty, isEnable3rdPartyBilling = string.Empty;
            int isMultiOffice = -1;

            CommandInfo cmdInfo = HHADataAccess.GetCommandDetails("DataMigration.Release.GetMigrationValidationDetails");
            if (cmdInfo == null)
            {
                throw new NullReferenceException("CommandInfo not found for GetMigrationValidationDetails()");
            }

            using (SqlConnection con = new SqlConnection(AppSettings.Instance[cmdInfo.ConnectionString]))
            {
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandTimeout = cmdInfo.CommandTimeout;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = cmdInfo.ProcName;
                    cmd.Parameters.AddWithValue("@UserID", userID);
                    cmd.Parameters.AddWithValue("@PayerID", payerID);
                    cmd.Parameters.AddWithValue("@ProviderID", providerID);
                    cmd.Parameters.AddWithValue("@CallerInfo", "GetMigrationValidationDetails");

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            isMultiOffice = dataReader.GetValue<int>("MultiOffice");
                            isEnable3rdPartyBilling = dataReader.GetValue<string>("Enable3rdPartyBilling");
                            isEnableAreaRateg = dataReader.GetValue<string>("EnableAreaRate");
                        }
                    }
                }
            }

            if (isMultiOffice > 0)
            {
                isValidate = false;
                validation = "Have Multiple Office.\n";
            }

            if (!string.IsNullOrEmpty(isEnable3rdPartyBilling) && isEnable3rdPartyBilling.ToUpper() == "YES")
            {
                isValidate = false;
                validation = validation + " Enabled 3rd Party Billing.\n";
            }

            if (!string.IsNullOrEmpty(isEnableAreaRateg) && isEnableAreaRateg.ToUpper() == "YES")
            {
                isValidate = false;
                validation = validation + " Enabled Area Rate.\n";
            }

            return validation;
        }

        public DataSet GetTablesRecordCount(int userID, int payerID, int providerID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();

            CommandInfo cmdInfo = HHADataAccess.GetCommandDetails("DataMigration.Release.GetMigrationTablesRecordCount");
            if (cmdInfo == null)
            {
                throw new NullReferenceException("CommandInfo not found for GetTablesRecordCount()");
            }

            using (SqlConnection con = new SqlConnection(AppSettings.Instance[cmdInfo.ConnectionString]))
            {
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                   // da.SelectCommand
                    cmd.CommandTimeout = cmdInfo.CommandTimeout;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = cmdInfo.ProcName;
                    cmd.Parameters.AddWithValue("@UserID", userID);
                    cmd.Parameters.AddWithValue("@PayerID", payerID);
                    cmd.Parameters.AddWithValue("@ProviderID", providerID);
                    cmd.Parameters.AddWithValue("@CallerInfo", "GetTablesRecordCount");

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        //while (reader.Read())
                        //{
                        //    DataTable dt = new DataTable();
                        //    dt.Load(reader);
                        //    ds.Tables.Add(dt);
                        //}
                    }
                }
            }

            return ds;
        }
    }
}
