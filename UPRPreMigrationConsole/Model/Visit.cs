﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BulkDeleteApp.Data.Model
{
    public class tblVisits_UPR_BK
    {
        [Key]
        public long AutoID { get; set; }
        public long? VisitID { get; set; }
        public long? vContractChhaID { get; set; }
        public long? vContractID { get; set; }
        public long? PatientID { get; set; }
        public long? ProviderID { get; set; }
        public long? PayerID { get; set; }
        public long? OfficeID { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
